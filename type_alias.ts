type CarYear = number;
type CarType = string;
type CarModel = string;
type car = {
    year: CarYear,
    type: CarType,
    model: CarModel
}

const carYear: CarYear = 2001;
const carType: CarType = "Toyata";
const carModel: CarModel = "Corolla";

const car1: car = {
    year: carYear,
    type: carType,
    model: carModel
}
console.log(car1)