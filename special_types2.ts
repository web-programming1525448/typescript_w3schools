let w: unknown = 1;
w = "String"
w = {
    runANonExistentmethod: () => (
        console.log("I think therefore I am")
    )
} as {runANonExistentmethod: () => void}

if (typeof w == 'object' && w!== null){
    (w as {runANonExistentmethod: Function}).runANonExistentmethod()
}