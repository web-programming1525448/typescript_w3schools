interface Rectangle {
    width: number;
    height: number;
}

interface ColorredRectangle extends Rectangle {
    color: string
}

const rectangle: Rectangle = {
    width: 20,
    height: 10,
}
console.log(rectangle);

const colorredRectangle: ColorredRectangle = {
    width: 20,
    height: 10,
    color: "red"
}
console.log(colorredRectangle)